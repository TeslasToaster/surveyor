import smbus
import time

bus = smbus.SMBus(1)
time.sleep(2)

address = 0x39
POWER_ON = 0x30
CONTROL_REGISTRY = 0x00

try:
	bus.read_i2c_block_data(address, 0xA, 3)
except Exception as e:
	print(e)
