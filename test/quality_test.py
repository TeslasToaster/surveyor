import time
import sys
import smbus
bus = smbus.SMBus(1)
address = 0x58
reply_buffer = [0,0,0,0,0,0]
reply = [0,0]
bus.write_i2c_block_data(address, 0x20, [0x03])
time.sleep(.5)
while True:

    bus.write_i2c_block_data(address,0x20,[0x08])
    time.sleep(.6)
    block = bus.read_i2c_block_data(address,0)
    for i in range(0,6):
            reply_buffer[i]=block[i]
    for i in range(0,2):
            reply[i]=reply_buffer[i*3]
            reply[i]<<=8
            reply[i]|=reply_buffer[i*3+1]
    print ("Co2 = ", reply[0])
    print ("\n")
    print("TVOC = ", reply[1])
    print ("\n")
    time.sleep(.2)