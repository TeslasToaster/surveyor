import RPi.GPIO as GPIO
import time
import random

if __name__ == "__main__":

	RED_LED_PIN = 17 	#Pysical pin 11
	GREEN_LED_PIN = 18 	#Physical pin 12

	GPIO.setmode(GPIO.BCM)
	GPIO.setwarnings(False)

	GPIO.setup(RED_LED_PIN, GPIO.OUT)
	GPIO.setup(GREEN_LED_PIN, GPIO.OUT)

	greenPin = GPIO.PWM(GREEN_LED_PIN, 100)
	greenPin.start(0)

	redPin = GPIO.PWM(RED_LED_PIN, 100)
	redPin.start(0)

	try:
		for y in range(0,3):
			for x in range(0,50):
				greenPin.ChangeDutyCycle(x)
				redPin.ChangeDutyCycle(50 - x)
				time.sleep(.05)
			for x in range(0, 50):
				greenPin.ChangeDutyCycle(50 - x)
				redPin.ChangeDutyCycle(x)
				time.sleep(.05)
		GPIO.cleanup()
#		for x in range(0, 10):
#			GPIO.output(RED_LED_PIN, GPIO.HIGH)
#			GPIO.output(GREEN_LED_PIN, GPIO.HIGH)
#			time.sleep(.5 + random.uniform(0,1)
#			GPIO.output(RED_LED_PIN, GPIO.LOW)
#			GPIO.output(GREEN_LED_PIN, GPIO.LOW)
#			time.sleep(.5 + random.uniform(0,1))
	except (KeyboardInterrupt, SystemExit):
		GPIO.cleanup()
		quit()

