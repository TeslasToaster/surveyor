import sqlite3 as db
import datetime
import os

class DataManager(object):
	TABLES = ['light', 'acceleration', 'global_position', 'air_pressure', 'sound', 'air_quality']
	TABLE_COLUMNS = {
		'light': [
			{'field':'visible_lux', 'type':'REAL'},
			{'field':'infrared_lux', 'type':'REAL'}
		],
		'acceleration': [
			{'field':'x_acceleration', 'type':'REAL'},
			{'field':'y_acceleration', 'type':'REAL'},
			{'field':'z_acceleration', 'type':'REAL'},
			{'field':'orientation', 'type':'TEXT'}
		],
		'global_position': [
			{'field':'latitude', 'type':'REAL'},
			{'field':'longitude', 'type':'REAL'},
			{'field':'altitude_in_m', 'type':'REAL'},
			{'field':'number_of_satellites', 'type':'INTEGER'}
		],
		'air_pressure': [
			{'field':'pressure_pascals', 'type':'REAL'},
			{'field':'temperature_C', 'type':'REAL'},
			{'field':'altitude_in_meters', 'type':'REAL'}
		],
		'sound':[
			{'field':'volume_in_decibels', 'type':'REAL'}
		],
		'air_quality':[
			{'field':'e_co2_in_parts_p_million', 'type':'REAL'},
			{'field':'total_voc_in_parts_p_billion', 'type':'REAL'}
		]
	}
	def __init__(self, directory, file, logger):
		self._logger = logger
		self._directory = directory
		self._file = file
		self._fullPath = self._directory + ("/" if self._directory[-1:] != '/' else '') + self._file
		self._logger.writeToLog("Finding local database @: '" + self._fullPath + "'")
		self._missingTables = []

		if os.path.isfile(self._fullPath):
			self._logger.writeToLog("Found and connected.")
			self._db = db.connect(self._fullPath, check_same_thread=False, isolation_level=None)
			self._logger.writeToLog("Verifying tables.")
			self.initilizeDatabase(not self.verifyDatabaseTables())
		else:
			self._logger.writeToLog("Not found. Creating new database...", self._logger.WARNING_TAG_TEXT)
			self._db = db.connect(self._fullPath, check_same_thread=False, isolation_level=None)
			self._logger.writeToLog("\tCreating tables:")
			self.initilizeDatabase()

	def initilizeDatabase(self, onlyMissing=False):
		c = self._db.cursor()

		for i, table in enumerate((self._missingTables if onlyMissing else DataManager.TABLES)):
			columnString = self.generateColumnString(table)
			c.execute("CREATE TABLE IF NOT EXISTS " + table + " ('time_stamp' DATETIME, " + columnString + ")")
			self._logger.writeToLog("\t\t'" + table + "' created.")

		self._db.commit()

	def generateColumnString(self, table):
		query = ""
		for i, column in enumerate(DataManager.TABLE_COLUMNS[table]):
			query += "'" + column['field'] + "' " + column['type'] + (" " if i == (len(DataManager.TABLE_COLUMNS[table])-1) else ", ")
		return query

	def verifyDatabaseTables(self):
		isValid = True
		c = self._db.cursor()

		missingTableCount = 0
		retreievedTables = []
		for table_name in c.execute("SELECT tbl_name FROM sqlite_master WHERE type='table'"):
			retreievedTables.append(table_name[0])

		for validTables in range(0, len(DataManager.TABLES)):
			if DataManager.TABLES[validTables] not in retreievedTables:
				self._missingTables.append(DataManager.TABLES[validTables])
				missingTableCount += 1
				isValid = False

		if not isValid:
			self._logger.writeToLog("\t" + str(missingTableCount) + " default table(s) not found. Creating...", self._logger.WARNING_TAG_TEXT)

		return isValid

	def writeToDatabase(self, table, values):
		c = self._db.cursor()
		columnLength = len(DataManager.TABLE_COLUMNS[table]) + 1
		values = (datetime.datetime.today().strftime('%Y-%B-%d %H:%M:%S.%f'), ) + values
		c.execute('INSERT INTO ' + table + ' VALUES (' + ("?,"*columnLength)[:-1] + ')', values)

		self._db.commit()

	def stop(self):
		self._logger.writeToLog("Closing database connection.")
		self._db.commit()
		self._db.close()