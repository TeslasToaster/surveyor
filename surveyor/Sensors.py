import smbus
import time

class i2CDevice(object):
    def __init__(self, address, logger, data_manager, name):
        self._bus = smbus.SMBus(1)
        self._address = address
        self._logger = logger
        self._dataManager = data_manager
        self._name = name

        self._logger.writeToLog("\t" + self._name + ": Powering ON.")
    
    def writeToDevice(self, register, value):
        self._bus.write_byte_data(self._address, register, value)
    
    # Returns an array of bytes
    def readFromDevice(self, register, numberOfBytes = 1):
        return self._bus.read_i2c_block_data(self._address, register, numberOfBytes)

class TSL2561(i2CDevice):
    NAME = "TSL2561"
    TABLE_NAME = 'light'
    I2C_ADDRESS = 0x39

    FASTEST_CLOCK_RATE_BYTE = 0x00
    FASTER_CLOCK_RATE_BYTE = 0x01
    NORMAL_CLOCK_RATE_BYTE = 0x02

    POWER_ON_BYTE = 0x03
    COMMAND_BYTE = 0x80

    CHANNEL_1_REG = 0x0C
    CHANNEL_2_REG = 0x0E

    def __init__(self, address, logger, data_manager, clock=NORMAL_CLOCK_RATE_BYTE):
        super().__init__(address, logger, data_manager, TSL2561.NAME)
        self._clock = clock
        
        # Set power mode on
        self.writeToDevice(0x00 | TSL2561.COMMAND_BYTE, TSL2561.POWER_ON_BYTE)

        '''
            Set clock rate of sensor
            BYTE        RATIO            TIME
            0x00(00)    val / .034	     Nominal integration time = 13.7ms
            0x01(01)	val / .252       Nominal integration time = 101ms
            0x02(02)	val / 1          Nominal integration time = 402ms
        '''

        # Check bounds of clock rate, default to normal
        if self._clock > TSL2561.NORMAL_CLOCK_RATE_BYTE or self._clock < TSL2561.FASTEST_CLOCK_RATE_BYTE:
            self._logger.writeToLog("\t\t" + TSL2561.NAME + ": Out of bounds clock rate. Defaulting to NORMAL_CLOCK_RATE (0x02)", self._logger.WARNING_TAG_TEXT)
            self._clock = TSL2561.NORMAL_CLOCK_RATE_BYTE

        # Establish ratio for clock rate displacements
        if self._clock == TSL2561.FASTEST_CLOCK_RATE_BYTE:
            self._denom = .034
            self._logger.writeToLog("\t\t" + TSL2561.NAME + ": Set to FASTEST_CLOCK_RATE.")
        elif self._clock == TSL2561.FASTER_CLOCK_RATE_BYTE:
            self._logger.writeToLog("\t\t" + TSL2561.NAME + ": Set to FASTER_CLOCK_RATE.")
            self._denom = .252
        else:
            self._logger.writeToLog("\t\t" + TSL2561.NAME + ": Set to NORMAL_CLOCK_RATE.")
            self._denom = 1

        self.writeToDevice(0x01 | TSL2561.COMMAND_BYTE, self._clock)

    def getInfo(self):
        # Get channel 1 data. (2 bytes)
        # ch0 LSB, ch0 MSB
        dat = self.readFromDevice(TSL2561.CHANNEL_1_REG | TSL2561.COMMAND_BYTE, 2)

        # Get channel 2 data. (2 bytes)
        # ch1 LSB, ch1 MSB
        dat2 = self.readFromDevice(TSL2561.CHANNEL_2_REG | TSL2561.COMMAND_BYTE, 2)

        # Convert the data
        # (VISIBLE, INFRARED)
        luxData = (int((((dat[1] * 0x100 + dat[0]) - (dat2[1] * 0x100 + dat2[0]))/self._denom)), int(dat2[1] * 0x100 + dat2[0]/self._denom))

        self._dataManager.writeToDatabase(TSL2561.TABLE_NAME, luxData)
        return luxData

class MMA8451(i2CDevice):
    NAME = "MMA8451"
    TABLE_NAME = 'acceleration'
    I2C_ADDRESS = 0x1D
    
    EARTH_GRAVITY_MS2 = 9.80665

    HIGH_RES_PRECISION = 14
    LOW_RES_PRECISION = 8

    RANGE_DIVIDER = {
        0x00: 4096 / EARTH_GRAVITY_MS2,
        0x01: 2048 / EARTH_GRAVITY_MS2,
        0x02: 1024 / EARTH_GRAVITY_MS2,
    }

    RANGE_2G_BYTE = 0x00
    RANGE_4G_BYTE = 0x01
    RANGE_8G_BYTE = 0x02

    BW_RATE_800HZ_BYTE = 0x0
    BW_RATE_400HZ_BYTE = 0x1
    BW_RATE_200HZ_BYTE = 0x2
    BW_RATE_100HZ_BYTE = 0x3
    BW_RATE_50HZ_BYTE = 0x4

    RESET_BYTE = 0x40

    ORIENTATION_ON_BYTE = 0x40

    CTRL_1_REG = 0x2A
    CTRL_2_REG = 0x2B
    CTRL_4_REG = 0x2D
    CTRL_5_REG = 0x2E

    PL_CFG_REG = 0x11

    XYZ_DATA_CFG_REG = 0x0E

    ORIENTATION_POS = {
        0: 'Portrait Up Front',     # Standard position, front facing sky
        1: 'Portrait Up Back',      # Standard position, front facing ground
        2: 'Portrait Down Front',   # 180-y, front facing sky (upside-down)
        3: 'Portrait Down Back',    # 180-y, front facing ground (upside-down)
        4: 'Landscape Right Front', # Clockwise turn, standard position, front facing sky
        5: 'Landscape Right Back',  # Clockwise turn, standard position, front facing ground
        6: 'Landscape Left Front',  # Anti-clockwise, front facing sky
        7: 'Landscape Left Back',   # Anti-clockwise, front facing ground
    }

    # Main data pointer. MUST read [x(msb, lsb), y(msb, lsb), z(msb, lsb)] bytes sequentially 
    DATA_OUT_X_MSB_REG = 0x01

    def __init__(self, address, logger, data_manager, sensor_range=RANGE_4G_BYTE, data_rate=BW_RATE_400HZ_BYTE):
        super().__init__(address, logger, data_manager, MMA8451.NAME)

        self._sensorRange = sensor_range
        self._dataRate = data_rate
        self.high_res_mode = 0x1

        self._logger.writeToLog("\t\t" + MMA8451.NAME + ": Set to BW_RATE_400HZ_BYTE.")

        # Reset sensor
        self.writeToDevice(MMA8451.CTRL_2_REG, MMA8451.RESET_BYTE)

        # Wait for sensor to be ready. Pretty random timeout. * What's the minimal time?
        time.sleep(.3)

        # Set resolution to 14-bit
        self.writeToDevice(MMA8451.CTRL_2_REG, 0x2)

        # Set device to read state
        self.writeToDevice(MMA8451.CTRL_4_REG, 0x1)
        self.writeToDevice(MMA8451.CTRL_5_REG, 0x1)

        # Turn on orientation
        self.writeToDevice(MMA8451.PL_CFG_REG, MMA8451.ORIENTATION_ON_BYTE)

        # Low noise activate
        self.writeToDevice(MMA8451.CTRL_1_REG, 0x4 | 0x1)

        # Set range
        self._sensorRange = self._sensorRange & 3
        reg1 = self.readFromDevice(MMA8451.CTRL_1_REG)[0]

        self.writeToDevice(MMA8451.CTRL_1_REG, 0x0)
        self.writeToDevice(MMA8451.XYZ_DATA_CFG_REG, self._sensorRange)
        self.writeToDevice(MMA8451.CTRL_1_REG, reg1 | 0x1)


        # Set data rate
        current = self.readFromDevice(MMA8451.CTRL_1_REG)[0]
        self.writeToDevice(MMA8451.CTRL_1_REG, 0x0)
        current &= 0xc7
        current |= (self._dataRate << 3)
        self.writeToDevice(MMA8451.CTRL_1_REG, current | 0x1)

    def getInfo(self):
        xyzData = self.readFromDevice(MMA8451.DATA_OUT_X_MSB_REG, 6)

        x = ((xyzData[0] << 8) | xyzData[1]) >> 2
        y = ((xyzData[2] << 8) | xyzData[3]) >> 2
        z = ((xyzData[4] << 8) | xyzData[5]) >> 2

        precision = MMA8451.HIGH_RES_PRECISION

        max_val = 2 ** (precision - 1) - 1
        signed_max = 2 ** precision

        x -= signed_max if x > max_val else 0
        y -= signed_max if y > max_val else 0
        z -= signed_max if z > max_val else 0

        x = round((float(x)) / MMA8451.RANGE_DIVIDER[self._sensorRange], 3)
        y = round((float(y)) / MMA8451.RANGE_DIVIDER[self._sensorRange], 3)
        z = round((float(z)) / MMA8451.RANGE_DIVIDER[self._sensorRange], 3)

        o = (self.readFromDevice(0x10)[0] & 0x7)

        accelData = (x, y, z, MMA8451.ORIENTATION_POS[o])
        self._dataManager.writeToDatabase(MMA8451.TABLE_NAME, accelData)

        # (X, Y, Z, ORIENTATION)
        return accelData

class MPL3115A2(i2CDevice):
    NAME = "MPL3115A2"
    TABLE_NAME = 'air_pressure'
    I2C_ADDRESS = 0x60

    ALTIMETER_READ_DATA_LENGTH = 6
    BAROMETER_READ_DATA_LENGTH = 4

    ALTIMETER_MODE_BYTE = 0xB9
    BAROMETER_MODE_BYTE = 0x39
    DATA_READY_BYTE = 0x07

    DATA_READ_REG = 0x00
    CTRL_REG = 0x26
    DATA_CONFIG_REG = 0x13

    def __init__(self, address, logger, data_manager):
        super().__init__(address, logger, data_manager, MPL3115A2.NAME)

        self.writeToDevice(MPL3115A2.CTRL_REG, MPL3115A2.ALTIMETER_MODE_BYTE)
        self.writeToDevice(MPL3115A2.DATA_CONFIG_REG, MPL3115A2.DATA_READY_BYTE)
        self.writeToDevice(MPL3115A2.CTRL_REG, MPL3115A2.ALTIMETER_MODE_BYTE)

    def getInfo(self):
        data = self.readFromDevice(MPL3115A2.DATA_READ_REG, MPL3115A2.ALTIMETER_READ_DATA_LENGTH)
        
        tHeight = ((data[1] * 65536) + (data[2] * 256) + (data[3] & 0xF0)) / 16
        temp = ((data[4] * 256) + (data[5] & 0xF0)) / 16
        altitude = tHeight / 16.0
        cTemp = temp / 16.0

        self.writeToDevice(MPL3115A2.CTRL_REG, MPL3115A2.BAROMETER_MODE_BYTE)
        data = self.readFromDevice(MPL3115A2.DATA_READ_REG, MPL3115A2.BAROMETER_READ_DATA_LENGTH)
        pres = ((data[1] * 65536) + (data[2] * 256) + (data[3] & 0xF0)) / 16
        pressure = (pres / 4.0) / 1000.0

        self._dataManager.writeToDatabase(MPL3115A2.TABLE_NAME, (pressure, cTemp, altitude))
        return (pressure, cTemp, altitude)

class SGP30(i2CDevice):
    NAME = "SGP30"
    TABLE_NAME = 'air_quality'
    I2C_ADDRESS = 0x58

    INIT_AIR_QUALITY_BYTES = 0x03
    MEASURE_AIR_QUALITY_BYTES = 0x08

    CTRL_REG = 0x20

    def __init__(self, address, logger, data_manager):
        super().__init__(address, logger, data_manager, SGP30.NAME)
        self._dataBuffer = [0,0,0,0,0,0]
        self._realData = [0,0]

        self.writeToDevice(SGP30.CTRL_REG, SGP30.INIT_AIR_QUALITY_BYTES)
        time.sleep(.5)

    def getInfo(self):
        self.writeToDevice(SGP30.CTRL_REG, SGP30.MEASURE_AIR_QUALITY_BYTES)
        time.sleep(.6)

        while len(self.readFromDevice(0x00)) != 1:
            reply = self.readFromDevice(0x00)
            print(reply)
            for i in range(0,6):
                self._dataBuffer[i] = reply[i]

            for i in range(0,2):
                    self._realData[i] = self._dataBuffer[i*3]
                    self._realData[i] <<= 8
                    self._realData[i] |= self._dataBuffer[i*4]
            
            time.sleep(.2)

            self._dataManager.writeToDatabase(SGP30.TABLE_NAME, (self._realData[0], self._realData[1]))
            return (self._realData[0], self._realData[1])

class GPS(object):
    NAME = "GPS"
    TABLE_NAME = 'global_position'
    ADDRESS = "/dev/ttyUSB0"
    def __init__(self, address, logger, data_manager):
        import serial
        self._address = address
        self._logger = logger
        self._dataManager = data_manager
        self._name = GPS.NAME

        self._logger.writeToLog("\tEstablishing communication with " + self._name + ".")
        self._gps = serial.Serial(self._address, baudrate = 9600)
        self._logger.writeToLog("\tEstablished.")

    def getInfo(self):
        lat = None
        lon = None
        speed = None
        noOfSatellites = 0
        altitude = None
        add = False

        line = self._gps.readline().decode("utf-8")
        data = line.split(",")
        if data[0] == "$GPGGA":
            if int(data[6]) >= 1:
                data[2] = data[2][0:2] + '.' + data[2][2:len(data[2])].replace('.', '')
                data[4] = data[4][0:2] + '.' + data[4][2:len(data[4])].replace('.', '')

                lat = data[2] if data[3] == 'N' else '-' + data[2]
                lon = data[4] if data[5] == 'E' else '-' + data[4]
                noOfSatellites = float(data[7])
                altitude = float(data[9])
                add = True
        if add:
            self._dataManager.writeToDatabase(GPS.TABLE_NAME, (lat, lon, altitude, noOfSatellites))
            return (lat, lon, altitude, noOfSatellites)
