import datetime
import os

class Logger(object):
    WARNING_TAG_TEXT = "[WARNING]"
    ERROR_TAG_TEXT = "[ERROR]"
    def __init__(self):
        self._projectPath = os.path.join(os.path.dirname( __file__ ), '..', 'logs')
        self._year = datetime.datetime.today().strftime('%Y')
        self._month = datetime.datetime.today().strftime('%B')
        self._day = datetime.datetime.today().strftime('%d')

        self._loggingDirectory = self._projectPath + "/" + self._year + "/" + self._month + "/" + self._day + "/"
        self._loggingFile = "system.log"

    def writeToLog(self, message, tag=None):
        if not os.path.exists(self._loggingDirectory):
            os.makedirs(self._loggingDirectory)

        with open(self._loggingDirectory + self._loggingFile, 'a') as myfile:
            myfile.write(("\n"+tag+" " if tag else "") + datetime.datetime.today().strftime('%H:%M:%S.%f')[:-3] + "\t" +  message + "\n" + ("\n" if tag else ""))

        myfile.close()

    def stop(self):
        pass