import RPi.GPIO as GPIO
from time import sleep
from threading import Thread

def _blinkingThread(StatusLED, rate):
    while StatusLED._blinking:
        if StatusLED._currentlyOn:
            GPIO.output(StatusLED._ledPin, GPIO.LOW)
            StatusLED._currentlyOn = False
        else:
            GPIO.output(StatusLED._ledPin, GPIO.HIGH)
            StatusLED._currentlyOn = True

        sleep(1/rate)

class StatusLED(object):
    def __init__(self, pinNumber):
        self._ledPin = pinNumber
        self._currentlyOn = False
        self._blinking = False
        self._blinkingThread = None

        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

        GPIO.setup(self._ledPin, GPIO.OUT)

        ## For Pulse Width
        # redPin = GPIO.PWM(RED_LED_PIN, 100)
	    # redPin.start(0)
        # greenPin.ChangeDutyCycle(x)

    def solidOn(self):
        GPIO.output(self._ledPin, GPIO.HIGH)
        self._currentlyOn = True
        self._blinking = False
        if self._blinkingThread:
            self._blinkingThread.join()

    def blink(self, rate):
        self._blinking = True
        self._blinkingThread = Thread(target = _blinkingThread, args = (self, rate))
        self._blinkingThread.start()

    def solidOff(self):
        GPIO.output(self._ledPin, GPIO.LOW)
        self._currentlyOn = False
        self._blinking = False
        if self._blinkingThread:
            self._blinkingThread.join()
    
    def stop(self):
        GPIO.cleanup()
