from .Logger import Logger
from .StatusLED import StatusLED
from .Sensors import *
from .DataManager import DataManager
from threading import Thread
import time

DEVICE_OK_LED_PIN = 18
DEVICE_ISSUE_LED_PIN = 17

DEFAULT_BLINK_RATE = 25

ACCELEROMETER_SENSOR_I2C_ADDRESS = 0x1D

def threaded_getInfo(surveyor, device):
    while surveyor.devices[device._name]['IS_OK']:
        device.getInfo()


class Surveyor(object):
    def __init__(self):
        self._dataDirectory = "./dat/"
        self._databaseFile = "data.db"
        self._logger = Logger()
        
        self.devices = {
            'TSL2561': {'IS_OK': True, 'device': {}},
            'MMA8451': {'IS_OK': True, 'device': {}},
            'MPL3115A2': {'IS_OK': True, 'device': {}},
            'SGP30': {'IS_OK': True, 'device': {}},
            'GPS': {'IS_OK': True, 'device': {}}
        }
        self.activeThreads = []
        
        # Create important instances
        self._dataManager = DataManager(self._dataDirectory, self._databaseFile, self._logger)

        # Create misc. instances
        self._statusOutput = StatusLED(DEVICE_OK_LED_PIN)

    def start(self):
        self._logger.writeToLog("Starting.")
        self._statusOutput.blink(DEFAULT_BLINK_RATE)
        self._logger.writeToLog("Initilizing devices...")

        # Get all flashy with the LED
        time.sleep(1)

        try:
            # Light Sensor
            self.devices[TSL2561.NAME]['device'] = TSL2561(TSL2561.I2C_ADDRESS, 
                logger=self._logger,
                data_manager=self._dataManager,
                clock=TSL2561.NORMAL_CLOCK_RATE_BYTE)

            # Accelerometer
            self.devices[MMA8451.NAME]['device'] = MMA8451(MMA8451.I2C_ADDRESS, 
                logger=self._logger,
                data_manager=self._dataManager)

            # GPS
            self.devices[GPS.NAME]['device'] = GPS(GPS.ADDRESS, 
                logger=self._logger,
                data_manager=self._dataManager)

            # Barometric Pressure Sensor
            self.devices[MPL3115A2.NAME]['device'] = MPL3115A2(MPL3115A2.I2C_ADDRESS, 
                logger=self._logger,
                data_manager=self._dataManager)
            
            # Air quality
            self.devices[SGP30.NAME]['device'] = SGP30(SGP30.I2C_ADDRESS, 
                logger=self._logger,
                data_manager=self._dataManager)

            # @TODO Microphone to detect ambient noise levels

            # Indicate to user that device has started successfully
            self._statusOutput.solidOn()
            self._logger.writeToLog("Done.")
            self._logger.writeToLog("Succesfully started.")
            return True

        except Exception as e:
            self._logger.writeToLog("Couldn't initialize sensors. (" + str(e) + ")", self._logger.ERROR_TAG_TEXT)
            self.displayIssue()
            return False

    def collectData(self):
        for device in self.devices:
            newThread = Thread(target = threaded_getInfo, args = (self, self.devices[device]['device']))
            self.activeThreads.append(newThread)
            newThread.start()

        while True:
            pass

    def displayIssue(self):
        self._statusOutput.solidOff()
        self._statusOutput.stop()
        self._statusOutput = StatusLED(DEVICE_ISSUE_LED_PIN)
        self._statusOutput.solidOn()

    def stop(self, forced=False):
        if forced:
            self._logger.writeToLog("Force stopping due to user.\n", self._logger.WARNING_TAG_TEXT)
        else:
            self._logger.writeToLog("Stopping...\n")

        for device in self.devices:
            self.devices[device]['IS_OK'] = False

        self._logger.writeToLog("Waiting for threads to finish.")
        for x in range (0, len(self.activeThreads)):
            self.activeThreads[x].join()

        # Stop all important instances
        self._logger.writeToLog("Closing database connection.")  
        self._dataManager.stop()

        # Stop all misc. instances
        self._logger.writeToLog("Cleaning GPIO pins.")
        self._statusOutput.solidOff()
        self._statusOutput.stop()

        # Final write to system log
        self._logger.writeToLog("Stopped.\n********************\n")
        self._logger.stop()

        # Goodbyte
        quit()