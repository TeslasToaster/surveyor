from surveyor.Surveyor import Surveyor

if __name__ == "__main__":
    try:
        surveyor = Surveyor()
        if surveyor.start():
            print('GO')
            surveyor.collectData()
            surveyor.stop()
        else:
            print('STOP')
            surveyor.stop()
    except KeyboardInterrupt:
        surveyor.stop(True)