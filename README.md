# Surveyor

To be used with a raspberry pi 3.
Below references pinout image in repository.

All sensors should be connected to the 3.3v power pin (pin 1) as well as a ground (pin 6).
i2c communication will be handled by pin 3 (data) and pin 5 (clock).

**The clock and data buses should be connected in parallel.**

The LEDs (preferable red and green) should be connected to pins 17 and 18 respectively.

There are logs that are automatically generated in the directory scheme of `logs/YYYY/Month/DD/system.log`.

The output destination is a sqlite database in `dat/data.db`.
The table structure is defined in `surveyor/DataManager.py`.

Pre-setup would require modifying `/etc/profile` and adding the lines at the very bottom:

``` python
sudo python3 '/path/to/repository/surveyor.py'
```

Typical use would be to connect to a 5v power source inside a vehicle. 
From there, be sure the light, gps, and air quality sensors are enclosed,
yet out of the vehicle. (Could probably 3-D print an enclosure).

The LEDs will both initially start blinking until device is ready. Then a 
solid green light will indicate that the device is ready and logging.
